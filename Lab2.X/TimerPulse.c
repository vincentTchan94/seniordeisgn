//****************************************************************************
//Filename:		TimerPulse.c
//Author:		Vincent Chan
//Date:  		10/4/2017
//Version:		1.0
//Device:		16f18324
//Description:	Blink an LED using a timer
//Complier:		XC8
//
//****************************************************************************

//****************************************************************************
// Includes
//****************************************************************************
#include <xc.h>
#include <stdint.h>
#include "config.h" //Include config file

//****************************************************************************
// Prototype functions
//****************************************************************************
void interrupt interruptRoutine(void);
void init(void);

//****************************************************************************
// main
//****************************************************************************
void main(void)
{     
    init();
    while (1)
    {
    }
}

//****************************************************************************
// Interrupt
//****************************************************************************
void interrupt interruptRoutine(void) {
    //If timer0 hits overflow, toggle LED
    if(PIE0bits.TMR0IE && PIR0bits.TMR0IF) {
	LATC5 = 1;		//Toggle LED
	_delay(7); 		//Delay 9uS (delay function offset by 1)
	LATC5 = 0;
	PIR0bits.TMR0IF = 0;	//Clear overflow flag
    }
}

//****************************************************************************
// Init
//****************************************************************************
void init(void) {
    //Set system clock up
    OSCCON1 = 0x60; 		// HFINTOSC   
    OSCFRQ = 0x03;  		// HFFRQ 4_MHz

    //Set LED to Out and pushbutton to in
    TRISC = 0b11011111; 	// LED on RC5 set to output

    //Enable interrupts
    INTCONbits.GIE = 1;
    INTCONbits.PEIE = 1;
    PIE0bits.TMR0IE = 1; 	//Timer interrupt enable

    //Timer0 configuration
    T0CON1bits.T0CS = 0b010; 	//Timer clock source set to HFINTOSC/4
    T0CON1bits.T0CKPS = 0x6; 	//Prescale 1/64
    T0CON0bits.T016BIT = 0;	//Set Timer0 to 8bit mode
    T0CON0bits.T0EN = 1; 	// Enable Timer 1
    TMR0H = 140;	 	//Set 140 as compare register
}


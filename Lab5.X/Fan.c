//****************************************************************************
//Filename:		PSM.c
//Author:		Vincent Chan
//Date:  		10/4/2017
//Version:		1.0
//Device:		16f18324
//Description:	PWM lab
//Complier:		XC8
//
//****************************************************************************

//****************************************************************************
// Includes
//****************************************************************************
#include <xc.h>
#include <stdint.h>
#include "config.h" //Include config file

//****************************************************************************
// Prototype functions
//****************************************************************************
void interrupt interruptRoutine(void);
void init(void);
void initFan();

#define AD90 0x36
#define AD150 0x2B
#define MAXVAL (0x36-0x2B)

uint8_t initT;
uint8_t curVal; 
//****************************************************************************
// main
//****************************************************************************
void main(void)
{     
    init();
    initFan();
    ADGO = 1;
    while (1)
    {
    }
}

//****************************************************************************
// Interrupt
//****************************************************************************
void interrupt interruptRoutine(void) {
    //If timer0 hits overflow, toggle LED
    if(PIE0bits.TMR0IE && PIR0bits.TMR0IF) {
	    TMR0IF = 0;
        curVal = ADRESH;
        if(initT != 0) {
            initT--; return;
        }
        if(ADRESH > AD90) {
            PWM5DCH = 0x7f;
            PWM5DCL = 0x00;
        }
        else {
            if(ADRESH < AD150) {
                PWM5DCH = 0xff;
                PWM5DCL = 0xc0;
            }
            else {
                uint8_t val = AD90 - ADRESH;
                //(x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
                PWM5DCH = ((val - AD90) * (0xff - 0x7f) / (MAXVAL - AD90)) + 0x7f;
            }
        }
	    ADGO = 1;
    }
}

//****************************************************************************
// Init
//****************************************************************************
void init(void) {
    //Set system clock up
    OSCCON1 = 0x60; 		// HFINTOSC   
    OSCFRQ = 0x03;  		// HFFRQ 4_MHz

    //Set RC2 as analog input (pot)
    TRISCbits.TRISC2 = 1;
    ANSELCbits.ANSC2 = 1;

    //Set up ADC
    ADCON0bits.CHS = 0b010010; //Set RC2 pin as channel
    ADCON1bits.ADFM = 0; //Left justified result
    ADCON1bits.ADCS = 0b101; //Clock select fosc/16
    ADCON0bits.ADON = 1;    //Enable adc

    //Setting up PWM
    TRISC = 0xFF; 	//disable output drivers on C
    TRISCbits.TRISC5 = 0;
    RC5PPS = 0b00010; 	//Set RC5 pin to PWM5 output 
    PPSLOCK = 1; 	//Lock the PPS
    PWM5EN = 1; //Enable pwm58
    
    //Timer2 (pwm timer) configuration
    PR2 = 0xff;
    PWMTMRSbits.P5TSEL = 0b01; //Set pwm5 to TMR2
    TMR2ON = 1;

    //Enable interrupts
    INTCONbits.GIE = 1;
    INTCONbits.PEIE = 1;
    PIE0bits.TMR0IE = 1; 	//Timer interrupt enable

    //Timer0 configuration
    T0CON1bits.T0CS = 0b010; 	//Timer clock source set to HFINTOSC/4
    T0CON1bits.T0CKPS = 0x6; 	//Prescale 1/64
    T0CON0bits.T016BIT = 0;	//Set Timer0 to 8bit mode
    T0CON0bits.T0EN = 1; 	// Enable Timer 1
}

void initFan(void) {
    initT = 255;
    PWM5DCH = 0xff;
    PWM5DCL = 0xc0;
    
    while(initT>0) {}
    
    PWM5DCH = 0x00;
    PWM5DCL = 0x00;
    
}
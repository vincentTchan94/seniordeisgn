//****************************************************************************
//Filename:		SPI.c
//Author:		Vincent Chan
//Date:  		10/4/2017
//Version:		1.0
//Device:		16f18324
//Description:	SPI lab
//Complier:		XC8
//
//****************************************************************************

//****************************************************************************
// Includes
//****************************************************************************
#include <xc.h>
#include <stdint.h>
#include "config.h" //Include config file

//****************************************************************************
// Prototype functions
//****************************************************************************
void interrupt interruptRoutine(void);
void init(void);
uint8_t shiftIn(uint8_t);
uint16_t map(uint16_t, uint16_t);

//****************************************************************************
// main
//****************************************************************************
void main(void)
{     
    init();
    ADGO = 1;
    
    while (1)  
    {
    }
}

//****************************************************************************
// Interrupt
//****************************************************************************
void interrupt interruptRoutine(void) {
    //If timer0 hits overflow, toggle LED
    if(PIE0bits.TMR0IE && PIR0bits.TMR0IF) {
	    TMR0IF = 0;
	    PWM5DCH = ADRESH;
	    PWM5DCL = ADRESL;

        uint8_t msg = 0;
        LATC0 = 0; //Turn on SS
       
        //Shift in first message
        //First Message:
        // Address [4 bit], CMD [2 bit], Data High [2 bit]
        msg |= 0x0 << 4; //Target wiper 0
        msg |= 0b00 << 2; //Set data to write
        msg = (ADRESH==0xFF)? msg|0b01 : msg&~(00); //Send first 2 bits of data
        shiftIn(msg);

        //Shift in second message
        //Second Message:
        // Data Low [8 bit]
        msg = (ADRESH==0xFF) ? 0 : ADRESH; //msg will get lowest 8 bits from data
        shiftIn(msg);
        LATC0 = 1; //Turn off SS

	    ADGO = 1; //Restart the adc
    }
}

//****************************************************************************
// Util Functions
//***************************************************************************
//shiftIn(): Shift data into the SPI device
uint8_t shiftIn(uint8_t data)
{
    SSP1CON1bits.WCOL = 0; //Clear Overwrite flag
    SSP1BUF = data;
    while(SSP1STATbits.BF == 0);
    return SSP1BUF;
}

//Map data to shift in. For the pot we work with,
//the highest value is 256, with a 0 value.
//Our pot supports 1024 values
uint16_t map(uint16_t val_high, uint16_t val_low) {
   uint16_t toMap = val_low;
   toMap |= (val_high<<8);

   return toMap * ((257) / (1024));
}

//****************************************************************************
// Init
//****************************************************************************
void init(void) {

    /********************************
     * Occupied Pins:
     * RA2 = ADC Input (pot input)
     * RC5 = LED Output
     * RC0 = SS
     * RC2 = MOSI
     * RC1 = SCK
     *******************************/
    //Set system clock up
    OSCCON1 = 0x60; 		// HFINTOSC  
    OSCFRQ = 0x03;  		// HFFRQ 4_MHz

    //Set RA2 as analog input (pot)
    TRISAbits.TRISA2 = 1;
    ANSELAbits.ANSA2 = 1;

    //Set up ADC
    ADCON0bits.CHS = 0b000010; //Set RA2 pin as channel
    ADCON1bits.ADFM = 0; //Left justified result
    ADCON1bits.ADCS = 0b101; //Clock select fosc/16
    ADCON0bits.ADON = 1;    //Enable adc

    //Setting up PWM
    TRISC = 0xFF; 	//disable output drivers on C
    TRISCbits.TRISC5 = 0;
    RC5PPS = 0b00010; 	//Set RC5 pin to PWM5 output 
    PPSLOCK = 1; 	//Lock the PPS
    PWM5EN = 1; //Enable pwm5
    
    //Timer2 (pwm timer) configuration
    PR2 = 0xff;
    PWMTMRSbits.P5TSEL = 0b01; //Set pwm5 to TMR2
    TMR2ON = 1;

    //Enable interrupts
    INTCONbits.GIE = 1;
    INTCONbits.PEIE = 1;
    PIE0bits.TMR0IE = 1; 	//Timer interrupt enable

    //Timer0 configuration
    T0CON1bits.T0CS = 0b010; 	//Timer clock source set to HFINTOSC/4
    T0CON1bits.T0CKPS = 0x6; 	//Prescale 1/64
    T0CON0bits.T016BIT = 0;	//Set Timer0 to 8bit mode
    T0CON0bits.T0EN = 1; 	// Enable Timer 1

    //Set up MSSP (SPI)
    SSP1CON1bits.SSPEN = 1;  //Enables serial port
    SSP1CON1bits.CKP = 1;    //Clock polarity idles on high
    SSP1CON1bits.SSPM = 0x2; //SPI Master, Fosc/64
    TRISCbits.TRISC0 = 0; LATC0 = 1; //SS idles high C0
    TRISCbits.TRISC2 = 0; RC2PPS = 0b11001; //MOSI C1
    TRISCbits.TRISC1 = 0; RC1PPS = 0b11000;     //SCK C2
}

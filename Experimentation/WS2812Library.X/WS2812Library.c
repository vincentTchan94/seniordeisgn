#include "WS2812Library.h"

void sendPixel(uint8_t green, uint8_t red, uint8_t blue) {
	//Send green component
	for(uint8_t i = 8; i > 0; i--)
		(green & (1 << i)) ? sendOne() : sendZero();
	//Send red component
	for(uint8_t i = 8; i > 0; i--)
		(red & (1 << i)) ? sendOne() : sendZero();
	//Send blue component
	for(uint8_t i = 8; i > 0; i--)
		(blue & (1 << i)) ? sendOne() : sendZero();
}

void sendOne() {
	//T1H = about 800 ns
	LATC5 = 1;
	asm("nop");
	asm("nop");
	asm("nop");
	asm("nop");
	asm("nop");
	asm("nop");
    asm("nop");
    asm("nop");
    asm("nop");
    asm("nop");

	//T1L = about 450 ns
	LATC5 = 0;
}

void sendZero() {
	//T0H = about 400
	LATC5 = 1;
	asm("nop");
	asm("nop");
    asm("nop");
    asm("nop");

	//T0L = about 850
	LATC5 = 0;
}


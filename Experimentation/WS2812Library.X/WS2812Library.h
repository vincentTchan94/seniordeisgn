/* 
 * File:   WS2812Library.h
 * Author: sasha
 *
 * Created on October 11, 2017, 4:54 PM
 */

#ifndef WS2812LIBRARY_H
#define	WS2812LIBRARY_H
#include <xc.h>
#include <stdint.h>

void sendPixel(uint8_t, uint8_t, uint8_t);
void sendOne();
void sendZero();

#endif	/* WS2812LIBRARY_H */


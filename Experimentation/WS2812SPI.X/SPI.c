//****************************************************************************
//Filename:		SPI.c
//Author:		Vincent Chan
//Date:  		10/4/2017
//Version:		1.0
//Device:		16f18324
//Description:	SPI lab
//Complier:		XC8
//
//****************************************************************************

//****************************************************************************
// Includes
//****************************************************************************
#include <xc.h>
#include <stdint.h>
#include "config.h" //Include config file
#include "defines.h"
//****************************************************************************
// Prototype functions
//****************************************************************************
void interrupt interruptRoutine(void);
void init(void);
uint8_t shiftIn(uint8_t);
uint16_t map(uint16_t, uint16_t);


int state = 0;
int i = 0;
int h = 0;
uint8_t hex[24];

//****************************************************************************
// main
//****************************************************************************
void main(void)
{     
    //green
    for(i = 0; i < 8; i++) {
        hex[i] = 1 & (HEX >> 15 - i);
    }
    //red
    for(i = 8; i < 16; i++) {
        hex[i] = 1 & (HEX >> 23 - (i - 8));
    }
    //BLUE
    for(i = 16; i < 24; i++) {
        hex[i] = 1 & ((HEX >> 7 - (i - 16)));
    }
    init();
    while (1)
    {
    }
}

//****************************************************************************
// Interrupt
//****************************************************************************
void interrupt interruptRoutine(void) {
    if (DOWN) {
        if (PIR0bits.TMR0IF) {
            TMR0IF = 0;
            /*
            state ^= 1;
            for(i = 0; i < 9; i++) shiftIn(0x00);
            if(state == 1) {
                for(h = 0; h < 8; h++) {
                for(i = 0; i < 8; i++)
                    shiftIn(0x80);
                for(i = 0; i < 8; i++)
                    shiftIn(0xE0);
                for(i = 0; i < 8; i++)
                    shiftIn(0x80);
                }
                for(h = 0; h < 8; h++){
                     for(i = 0; i < 8; i++)
                         shiftIn(0x80);
                     for(i=0; i < 16; i++)
                         shiftIn(0xE0);
                }
            }
            else {
                 for(h = 0; h < 8; h++){
                     for(i = 0; i < 8; i++)
                         shiftIn(0x80);
                     for(i=0; i < 16; i++)
                         shiftIn(0xE0);
                }
            }
            }
             * */
            state = (state + 1) % 32;
            for (i = 0; i < 32; i++) {
                if (i == state) {
                    for (h = 0; h < 24; h++)
                        if (hex[h]) shiftIn(0xE0);
                        else shiftIn(0x80);
                } else for (h = 0; h < 24; h++) shiftIn(0x80);
            }
        }
    }
}

//****************************************************************************
// Util Functions
//***************************************************************************
//shiftIn(): Shift data into the SPI device
uint8_t shiftIn(uint8_t data)
{
    SSP1CON1bits.WCOL = 0; //Clear Overwrite flag
    SSP1BUF = data;
    while(SSP1STATbits.BF == 0);
    return SSP1BUF;
}

//Map data to shift in. For the pot we work with,
//the highest value is 256, with a 0 value.
//Our pot supports 1024 values
uint16_t map(uint16_t val_high, uint16_t val_low) {
   uint16_t toMap = val_low;
   toMap |= (val_high<<8);

   return toMap * ((257) / (1024));
}

//****************************************************************************
// Init
//****************************************************************************
void init(void) {

    /********************************
     * Occupied Pins:
     * RA2 = ADC Input (pot input)
     * MOSI C2
     *******************************/
    //Set system clock up
    OSCCON1 = 0x00; 		// HFINTOSC   2xpll
    OSCFRQ = 0x04;  		// HFFRQ 16_MHz

    /*************
     Button inputs
     **************/
    //Down = C4
    TRISCbits.TRISC4 = 1;
    ANSELCbits.ANSC4 = 0;
    

    /***************
     POT input and ADC
     ****************/
    //Set RA2 as analog input (pot)
    TRISAbits.TRISA2 = 1;
    ANSELAbits.ANSA2 = 1;

    //Set up ADC
    ADCON0bits.CHS = 0b000010; //Set RA2 pin as channel
    ADCON1bits.ADFM = 0; //Left justified result
    ADCON1bits.ADCS = 0b101; //Clock select fosc/16
    ADCON0bits.ADON = 1;    //Enable adc

    
    

    //Enable interrupts
    INTCONbits.GIE = 1;
    INTCONbits.PEIE = 1;
    PIE0bits.TMR0IE = 1; 	//Timer interrupt enable

    //Timer0 configuration
    T0CON1bits.T0CS = 0b010; 	//Timer clock source set to HFINTOSC/4
    T0CON1bits.T0CKPS = 0xB; 	//Prescale 1/256
    T0CON0bits.T016BIT = 0;	//Set Timer0 to 8bit mode
    T0CON0bits.T0EN = 1; 	// Enable Timer 1

    //Set up MSSP (SPI) for WS2812 Comms
    SSP1CON1bits.SSPEN = 1;  //Enables serial port
    SSP1CON1bits.CKP = 1;    //Clock polarity idles on high
    SSP1CON1bits.SSPM = 0x0; //SPI Master, Fosc/4
    TRISCbits.TRISC2 = 0; RC2PPS = 0b11001; //MOSI C2
}

/* 
 * File:   defines.h
 * Author: sasha
 *
 * Created on November 5, 2017, 10:49 AM
 */

#ifndef DEFINES_H
#define	DEFINES_H

#define DOWN !PORTCbits.RC4

#define PUYO_ONE 0xFF1b7e
#define HEX 0x0082ff

#endif	/* DEFINES_H */


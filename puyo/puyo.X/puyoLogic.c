#include "puyoLogic.h"
#include <xc.h>

#ifdef DEBUG_CONSOLE
uint8_t main() {
    initBoard();
    printBoard();

    while(1) {
        uint8_t input;
        scanf(" %c", &input);
        processAction(input);
        printBoard();
    }
}
#endif

void initBoard() {
    uint8_t i;
    visitedFlag = VFLAG_MASK;

    for(i = 0; i < BOARDSIZE; i++) board[i] = 0;
    for(i = 0; i < BOARDSIZE-10; i += BOARDWIDTH) {
#ifdef DEBUG_CONSOLE
        printf("%d %d\n", i, i+BOARDWIDTH-1);
#endif
        board[i] = WALL;
        board[i+BOARDWIDTH-1] = WALL;
    }
    for(i; i < BOARDSIZE; i++) {
#ifdef DEBUG_CONSOLE
        printf("%d\n", i); 
#endif
        board[i] = WALL;
    }
    spawnPuyo();
}

void printBoard() {
#ifdef DEBUG_CONSOLE
    uint8_t i, h;
    uint8_t curLoc = 11;
    uint8_t orbitLoc;
    switch(aPuyo.state) {
        case ONTOP: orbitLoc = aPuyo.loc-BOARDWIDTH; break;
        case ONRIGHT: orbitLoc = aPuyo.loc+1; break;
        case ONBOTTOM: orbitLoc = aPuyo.loc+BOARDWIDTH; break;
        case ONLEFT: orbitLoc = aPuyo.loc-1; break;
    }
    for(i = 1; i < BOARDHEIGHT-1; i++) {
        for(h = 1; h < BOARDWIDTH-1; h++) {
            printf("%d ", (aPuyo.loc == curLoc)? aPuyo.color : 
                    (orbitLoc == curLoc)? aPuyo.orbCol :
                    board[curLoc]);
            curLoc++;
        }
        curLoc += 2;
        printf("\n");
    }
#else
    uint8_t i, h;
    uint8_t curLoc = 11;
    uint8_t orbitLoc;
    switch (aPuyo.state) {
        case ONTOP: orbitLoc = aPuyo.loc - BOARDWIDTH;
            break;
        case ONRIGHT: orbitLoc = aPuyo.loc + 1;
            break;
        case ONBOTTOM: orbitLoc = aPuyo.loc + BOARDWIDTH;
            break;
        case ONLEFT: orbitLoc = aPuyo.loc - 1;
            break;
    }
    for (i = 1; i < BOARDHEIGHT - 1; i++) {
        for (h = 1; h < BOARDWIDTH - 1; h++) {
            if(aPuyo.loc == curLoc) shiftIn(aPuyo.color);
            else if(orbitLoc == curLoc) shiftIn(aPuyo.orbCol);
            else shiftIn(board[curLoc]);
            curLoc++;
        }
        curLoc += 2;
    }
#endif
}

void spawnPuyo() {
    aPuyo.color = (rand() % 3) + 1;
    aPuyo.orbCol = (rand() % 3) + 1;
    aPuyo.state = ONTOP; 
    aPuyo.loc = SPAWNLOC;
    return;
}

uint8_t gameOver() {
    return (board[SPAWNLOC] != EMPTY);
}

void processAction(uint8_t act) {
    switch(act) {
        case ACTIONROTATERIGHT:
        case ACTIONROTATELEFT:
            while(1) {
                if(act == ACTIONROTATERIGHT) aPuyo.state = (aPuyo.state + 1) % 4;
                else aPuyo.state = (aPuyo.state-1) % 4;
                if(aPuyo.state == ONTOP) return;
                if(aPuyo.state == ONRIGHT && board[aPuyo.loc+1] == EMPTY) return;
                if(aPuyo.state == ONBOTTOM && board[aPuyo.loc+BOARDWIDTH] == EMPTY) return;
                if(aPuyo.state == ONLEFT && board[aPuyo.loc-1] == EMPTY) return;
            }
        case ACTIONRIGHT:
            if(aPuyo.state == ONRIGHT && board[aPuyo.loc+2] != EMPTY) return;
            else if (aPuyo.state == ONTOP && 
                    (board[aPuyo.loc+1] != EMPTY || board[aPuyo.loc-BOARDWIDTH+1] != EMPTY)) return;
            else if(aPuyo.state == ONBOTTOM &&
                    (board[aPuyo.loc+1] != EMPTY || board[aPuyo.loc+BOARDWIDTH+1] != EMPTY)) return;
            else if (board[aPuyo.loc+1] != EMPTY) return;
            aPuyo.loc++;
            return;
        case ACTIONLEFT:
            if(aPuyo.state == ONLEFT && board[aPuyo.loc-2] != EMPTY) return;
            else if (aPuyo.state == ONTOP && 
                    (board[aPuyo.loc-1] != EMPTY || board[aPuyo.loc-BOARDWIDTH-1] != EMPTY)) return;
            else if(aPuyo.state == ONBOTTOM &&
                    (board[aPuyo.loc-1] != EMPTY || board[aPuyo.loc+BOARDWIDTH-1] != EMPTY)) return;
            else if (board[aPuyo.loc-1] != EMPTY) return;
            aPuyo.loc--;
            return;
        case ACTIONDOWN:
            if(aPuyo.state == ONBOTTOM && board[aPuyo.loc+BOARDWIDTH+BOARDWIDTH] != EMPTY) {
                settlePuyo();
                return;
            }
            else if(aPuyo.state == ONRIGHT && 
                    (board[aPuyo.loc+BOARDWIDTH] != EMPTY || board[aPuyo.loc+1+BOARDWIDTH] != EMPTY)) {
                settlePuyo();
                return;
            }
            else if (aPuyo.state == ONLEFT && 
                    (board[aPuyo.loc+BOARDWIDTH] != EMPTY || board[aPuyo.loc-1+BOARDWIDTH] != EMPTY)) {
                settlePuyo();
                return;
            }
            else if(board[aPuyo.loc+BOARDWIDTH] != EMPTY) {
                settlePuyo();
                return;
            }
            aPuyo.loc += BOARDWIDTH;
            return;
        case ACTIONHARDDROP:
            settlePuyo();
            return;
    }
}

void settlePuyo() {
    uint8_t orbLoc;
    switch(aPuyo.state) {
        case ONTOP:
            while(board[aPuyo.loc+BOARDWIDTH] == EMPTY) aPuyo.loc += BOARDWIDTH;
            board[aPuyo.loc] = aPuyo.color;
            board[aPuyo.loc-BOARDWIDTH] = aPuyo.orbCol;
            while(popPuyo());
            spawnPuyo();
            return;
        case ONBOTTOM:
            orbLoc = aPuyo.loc+BOARDWIDTH;
            while(board[orbLoc+BOARDWIDTH] == EMPTY) orbLoc += BOARDWIDTH;
            board[orbLoc-BOARDWIDTH] = aPuyo.color;
            board[orbLoc] = aPuyo.orbCol;
            while(popPuyo());
            spawnPuyo();
            return;
        case ONRIGHT:
            orbLoc = aPuyo.loc+1;
            break;
        case ONLEFT:
            orbLoc = aPuyo.loc-1;
            break;
    }
    while(board[aPuyo.loc+BOARDWIDTH] == EMPTY) aPuyo.loc += BOARDWIDTH;
    while(board[orbLoc+BOARDWIDTH] == EMPTY) orbLoc += BOARDWIDTH;
    board[aPuyo.loc] = aPuyo.color;
    board[orbLoc] = aPuyo.orbCol;

    while(popPuyo());
    spawnPuyo();
    return;
}

uint8_t popPuyo() {
    //Implementing BFS, we will pop the puyos.
    uint8_t neighbors[20];
    uint8_t neighborCount = 0;
    uint8_t popped = 0;
    uint8_t h = 0;

    //Iterate through the array looking for neighbors
    uint8_t i = 10; //Start searching in playArea
    for (i; i < BOARDSIZE; i++) {
        if(isNotVisited(i) != 1 || board[i] == EMPTY) continue; //If visited or a wall, continue
        board[i] ^= VFLAG_MASK;
        neighborCount = 0;
        neighbors[neighborCount++] = i;
        uint8_t nS_i = 0;
        for(nS_i = 0; nS_i < neighborCount; nS_i++) {
            int8_t index = neighbors[nS_i];
            //Check up
            if(index>20) {
                if( ((board[index] & PUYO_MASK) == (board[index-BOARDWIDTH] & PUYO_MASK))
                        && (isNotVisited(index-BOARDWIDTH) == 1) ) {
                    board[index-BOARDWIDTH] ^= VFLAG_MASK;
                    neighbors[neighborCount++] = index-BOARDWIDTH;
                }
            }
            //Check right
            if( ((board[index] & PUYO_MASK) == (board[index+1] & PUYO_MASK))
                    && (isNotVisited(index+1) == 1) ) {
                board[index+1] ^= VFLAG_MASK;
                neighbors[neighborCount++] = index+1;
            }
            //Check left
            if( ((board[index] & PUYO_MASK) == (board[index-1] & PUYO_MASK))
                    && (isNotVisited(index-1) == 1) ) {
                board[index-1] ^= VFLAG_MASK;
                neighbors[neighborCount++] = index-1;
            }
            //Check down
            if( ((board[index] & PUYO_MASK) == (board[index+BOARDWIDTH] & PUYO_MASK))
                    && (isNotVisited(index+BOARDWIDTH) == 1) ) {
                board[index+BOARDWIDTH] ^= VFLAG_MASK;
                neighbors[neighborCount++] = index+BOARDWIDTH;
            }
        }
        if(neighborCount >= 4) {
            for(nS_i = 0; nS_i < neighborCount; nS_i++ ) {
                board[neighbors[nS_i]] = MARKED_POP;
                popped = 1;
            }
        }
    }
    for(i = 1; i < BOARDWIDTH - 1; i++) {
        for(h = i + (BOARDSIZE-BOARDWIDTH); h > BOARDWIDTH; h -= BOARDWIDTH) {
            while(board[h] == MARKED_POP) {
                uint8_t moveItr;
                for (moveItr = h; moveItr > BOARDWIDTH; moveItr -= BOARDWIDTH) {
                    board[moveItr] = (board[moveItr-BOARDWIDTH]);
                }
                board[moveItr] = 0;
            }
            board[h] &= ~VFLAG_MASK;
        }
    }
    return popped;
}

uint8_t isNotVisited(uint8_t index) {
    if(board[index] == WALL) { return WALL; }
    if((board[index] & PUYO_MASK) == EMPTY) return 4;
    if((board[index] & VFLAG_MASK ) == visitedFlag) return 0;
    return 1;
}

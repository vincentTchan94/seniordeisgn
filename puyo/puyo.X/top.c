//****************************************************************************
//Filename:		top.c
//Author:		Vincent Chan
//Date:  		11/6/2017
//Version:		1.0
//Device:		16f18324
//Description:	Top module for puyo game
//Complier:		XC8
//
//****************************************************************************

//****************************************************************************
// Includes
//****************************************************************************
#include <xc.h>
#include <stdint.h>
#include <stdlib.h>
#include "config.h" //Include config file
#include "defines.h"
#include "puyoLogic.h"

//****************************************************************************
// Prototype functions
//****************************************************************************
void interrupt interruptRoutine(void);
void init(void);
uint8_t shiftIn(uint8_t);

uint8_t state = 0;
uint8_t i = 0;
uint8_t h = 0;
uint8_t puyo1[24];
uint8_t puyo2[24];
uint8_t puyo3[24];
uint8_t debounceTime = 0;
uint8_t diff = 0xff;
uint8_t randSeed = 0;
uint8_t waitingForPlayer;

//****************************************************************************
// main
//****************************************************************************
void main(void)
{     
    while (1){
        init();
        ADGO = 1;
        initBoard();
        printBoard();
        waitingForPlayer = 1;
        srand((unsigned int) TMR0L);
        while(!gameOver());
    }
}

//****************************************************************************
// Interrupt
//****************************************************************************
void interrupt interruptRoutine(void) {
    if (PIR0bits.TMR0IF) {
        if(!waitingForPlayer) processAction(ACTIONDOWN);
        printBoard();
        TMR0IF = 0;
    }

    //When the debounce timer interrupts, turn off debouncer
    if(PIR1bits.TMR1IF) {
        PIR1bits.TMR1IF = 0;
        debounceTime = 0;
        DEBOUNCER_ON = 0;
    }

    //**
    //BUTTON PROCESSING
    //the buttons will process through edge detection
    //**
    if(BUTTON_ACTION) {
        BUTTON_ACTION = 0; //Reset flag
        if(waitingForPlayer && !debounceTime) {
            waitingForPlayer = 0;
            DOWN_ACTION = 0; //Reset flag
            RIGHT_ACTION = 0;
            LEFT_ACTION = 0;
            ROTATE_RIGHT_ACTION = 0;
            ROTATE_LEFT_ACTION = 0;
            HARD_DROP_ACTION = 0;
            return;
        }

        //DOWN button pressed
        if(DOWN_ACTION) {
            DOWN_ACTION = 0; //Reset flag
            if(DOWN_PRESSED) {
                TMR0H = diff / 4;
            }
            //Down is released
            else {
                TMR0H = diff;
            }
        }

        //RIGHT button pressed
        if(RIGHT_ACTION) {
            RIGHT_ACTION = 0;
            if(RIGHT_PRESSED) {
                DEBOUNCER_ON = 0;
                if(debounceTime) return;
                processAction(ACTIONRIGHT);
                printBoard();
                debounceTime = 1;
            }
            else {
                DEBOUNCER_ON = 1;
            }
        }

        //LEFT BUTTON PRESSED
        if(LEFT_ACTION) {
            LEFT_ACTION = 0;
            if(LEFT_PRESSED) {
                DEBOUNCER_ON = 0;
                if(debounceTime) return;
                processAction(ACTIONLEFT);
                printBoard();
                debounceTime = 1;
            }
            else {
                DEBOUNCER_ON = 1;
            }
        }

        //ROTATE BUTTON PRESSED
        if(ROTATE_RIGHT_ACTION) {
            ROTATE_RIGHT_ACTION = 0;
            if(ROTATE_RIGHT_PRESSED) {
                DEBOUNCER_ON = 0;
                if(debounceTime) return;
                processAction(ACTIONROTATERIGHT);
                printBoard();
                debounceTime = 1;
            }
            else {
                DEBOUNCER_ON = 1;
            }
        }

        //ROTATE BUTTON PRESSED
        if(ROTATE_LEFT_ACTION) {
            ROTATE_LEFT_ACTION = 0;
            if(ROTATE_LEFT_PRESSED) {
                DEBOUNCER_ON = 0;
                if(debounceTime) return;
                processAction(ACTIONROTATELEFT);
                printBoard();
                debounceTime = 1;
            }
            else {
                DEBOUNCER_ON = 1;
            }
        }

        //ROTATE BUTTON PRESSED
        if(HARD_DROP_ACTION) {
            HARD_DROP_ACTION = 0;
            if(HARD_DROP_PRESSED) {
                DEBOUNCER_ON = 0;
                if(debounceTime) return;
                processAction(ACTIONHARDDROP);
                printBoard();
                debounceTime = 1;
            }
            else {
                DEBOUNCER_ON = 1;
            }
        }


    }
}

//****************************************************************************
// Util Functions
//***************************************************************************
//shiftIn(): Shift data into the SPI device
uint8_t shiftIn(uint8_t puyo)
{
    uint8_t i = 0;
    switch(puyo) {
        case EMPTY:
            for(i = 0; i <24; i++) {
                SSP1CON1bits.WCOL = 0; //Clear Overwrite flag
                SSP1BUF = 0x80;
                while (SSP1STATbits.BF == 0);
            }
            break;
        case PONE:
            for(i = 0; i<24; i++) {
                SSP1CON1bits.WCOL = 0; //Clear Overwrite flag
                if(puyo1[i]) SSP1BUF = 0xE0;
                else SSP1BUF = 0x80;
                while (SSP1STATbits.BF == 0);
            }
            break;
        case PTWO:
            for(i = 0; i<24; i++) {
                SSP1CON1bits.WCOL = 0; //Clear Overwrite flag
                if(puyo2[i]) SSP1BUF = 0xE0;
                else SSP1BUF = 0x80;
                while (SSP1STATbits.BF == 0);
            }
            break;
        case PTHREE:
            for(i = 0; i<24; i++) {
                SSP1CON1bits.WCOL = 0; //Clear Overwrite flag
                if(puyo3[i]) SSP1BUF = 0xE0;
                else SSP1BUF = 0x80;
                while(SSP1STATbits.BF == 0);
            }
            break;       
    }
    return SSP1BUF;
}



//****************************************************************************
// Init
//****************************************************************************
void init(void) {

    /********************************
     * Occupied Pins:
     * RA2 = ADC Input (pot input)
     * MOSI C2
     *******************************/
    //Set system clock up
    OSCCON1 = 0x00; 		// HFINTOSC 2xpll
    OSCFRQ = 0x04;  		// HFFRQ 16_MHz

    /*************
     Button inputs
     **************/
    //Down = C4
    TRISCbits.TRISC4 = 1; //Set as input
    ANSELCbits.ANSC4 = 0; //Digital I/O
    IOCCNbits.IOCCN4 = 1; //Enable interrupts
    IOCCPbits.IOCCP4 = 1;

    //Right = A3
    //TRISAbits.TRISA3 = 1; //Set as input
    //ANSELAbits.ANSA3 = 0; //Digital I/O
    IOCANbits.IOCAN3 = 1; //Enable interrupts
    IOCAPbits.IOCAP3 = 1;

    //Left = A5
    TRISAbits.TRISA5 = 1; //Set as input
    ANSELAbits.ANSA5 = 0; //Digital I/O
    IOCANbits.IOCAN5 = 1; //Enable interrupts
    IOCAPbits.IOCAP5 = 1;

    //ROTATE = A0
    TRISAbits.TRISA0 = 1; //Set as input
    ANSELAbits.ANSA0 = 0; //Digital I/O
    IOCANbits.IOCAN0 = 1; //Enable interrupts
    IOCAPbits.IOCAP0 = 1;

    //ROTATE LEFT = A1
    TRISAbits.TRISA1 = 1; //Set as input
    ANSELAbits.ANSA1 = 0; //Digital I/O
    IOCANbits.IOCAN1 = 1; //Enable interrupts
    IOCAPbits.IOCAP1 = 1;

    //HARD DROP = A4
    TRISAbits.TRISA4 = 1; //Set as input
    ANSELAbits.ANSA4 = 0; //Digital I/O
    IOCANbits.IOCAN4 = 1; //Enable interrupts
    IOCAPbits.IOCAP4 = 1;




    /***************
     POT input and ADC
     ****************/
    /*
    //Set RC3 as analog input (pot)
    TRISCbits.TRISC3 = 1;
    ANSELCbits.ANSC3 = 1;

    //Set up ADC
    ADCON0bits.CHS = 0b010011; //Set RC3 pin as channel
    ADCON1bits.ADFM = 0; //Left justified result
    ADCON1bits.ADCS = 0b101; //Clock select fosc/16
    ADCON0bits.ADON = 1;    //Enable adc
     * */

    //Enable interrupts
    INTCONbits.GIE = 1;
    INTCONbits.PEIE = 1;
    PIE0bits.IOCIE = 1; //Enable Button press interrupts

    //Timer0 configuration
    T0CON1bits.T0CS = 0b010; 	//Timer clock source set to HFINTOSC/4
    //T0CON1bits.T0CKPS = 0xB; 	//Prescale 1/256
    T0CON1bits.T0CKPS = 0xD; //Prescale 1/1024
    T0CON0bits.T016BIT = 0;	//Set Timer0 to 8bit mode
    T0CON0bits.T0EN = 1; 	// Enable Timer 0
    PIE0bits.TMR0IE = 1; 	//Timer interrupt enable
    
    //Timer 1: Debouncer
    //Set up timer 1. Debounce at about 16ms
    PIE1bits.TMR1IE = 1;// enable interrupt for timer1

    //Set up MSSP (SPI) for WS2812 Comms
    SSP1CON1bits.SSPEN = 1;  //Enables serial port
    SSP1CON1bits.CKP = 1;    //Clock polarity idles on high
    SSP1CON1bits.SSPM = 0x0; //SPI Master, Fosc/4
    TRISCbits.TRISC2 = 0; RC2PPS = 0b11001; //MOSI C2
    
    
    /*Board Setup
     * Sets up the puyo hex codes
     */
    //Puyo 1
    //green
    for(i = 0; i < 8; i++) {
         puyo1[i] = 1 & (PUYO_ONE >> 15 - i);
    }
    //red
    for(i = 8; i < 16; i++) {
        puyo1[i] = 1 & (PUYO_ONE >> 23 - (i - 8));
    }
    //BLUE
    for(i = 16; i < 24; i++) {
        puyo1[i] = 1 & ((PUYO_ONE >> 7 - (i - 16)));
    }
    
    //Puyo 2
    //green
    for(i = 0; i < 8; i++) {
        puyo2[i] = 1 & (PUYO_TWO >> 15 - i);
    }
    //red
    for(i = 8; i < 16; i++) {
        puyo2[i] = 1 & (PUYO_TWO >> 23 - (i - 8));
    }
    //BLUE
    for(i = 16; i < 24; i++) {
        puyo2[i] = 1 & ((PUYO_TWO >> 7 - (i - 16)));
    }
    
    //Puyo 3
    //green
    for(i = 0; i < 8; i++) {
        puyo3[i] = 1 & (PUYO_THREE >> 15 - i);
    }
    //red
    for(i = 8; i < 16; i++) {
        puyo1[i] = 1 & (PUYO_THREE >> 23 - (i - 8));
    }
    //BLUE
    for(i = 16; i < 24; i++) {
        puyo1[i] = 1 & ((PUYO_THREE >> 7 - (i - 16)));
    }
}


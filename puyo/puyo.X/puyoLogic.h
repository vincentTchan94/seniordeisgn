#ifndef PUYOLOGIC_H
#define PUYOLOGIC_H

#ifdef DEBUG_CONSOLE
#include <inttypes.h>
#include <stdio.h>
#else
#include <stdint.h>
#endif

#define BOARDWIDTH 10
#define BOARDHEIGHT 10
#define BOARDSIZE 100
#define SPAWNLOC 15


#define PONE 1
#define PTWO 2
#define PTHREE 3

#define WALL 0b11000000
#define VFLAG_MASK 0x80
#define PUYO_MASK 0x03
#define MARKED_POP 0x84

#define ONTOP 0
#define ONRIGHT 1
#define ONBOTTOM 2
#define ONLEFT 3

#define ACTIONRIGHT 'd'
#define ACTIONLEFT 'a'
#define ACTIONDOWN 's'
#define ACTIONROTATELEFT 'x'
#define ACTIONROTATERIGHT 'z'
#define ACTIONHARDDROP 'w'

#define EMPTY 0

typedef struct {
    uint8_t color;
    uint8_t orbCol;
    uint8_t state;
    uint8_t loc;
}activePuyo;
uint8_t board[BOARDSIZE];
activePuyo aPuyo;
uint8_t visitedFlag;

#ifdef DEBUG_CONSOLE
uint8_t main();
#else
uint8_t shiftIn(uint8_t); //External
#endif
void printBoard();
void initBoard();
void spawnPuyo();
uint8_t gameOver();
void processAction(uint8_t);
void settlePuyo();
uint8_t popPuyo();
uint8_t isNotVisited(uint8_t);
//uint8_t isIn(uint8_t, uint8_t, uint8_t*);

#endif

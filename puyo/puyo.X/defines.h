/* 
 * File:   defines.h
 * Author: sasha
 *
 * Created on November 5, 2017, 10:49 AM
 */

#ifndef DEFINES_H
#define	DEFINES_H


#define BUTTON_ACTION PIR0bits.IOCIF
#define DOWN_ACTION IOCCFbits.IOCCF4
#define DOWN_PRESSED !PORTCbits.RC4
#define RIGHT_ACTION IOCAFbits.IOCAF3
#define RIGHT_PRESSED !PORTAbits.RA3
#define LEFT_ACTION IOCAFbits.IOCAF5
#define LEFT_PRESSED !PORTAbits.RA5
#define ROTATE_RIGHT_ACTION IOCAFbits.IOCAF0
#define ROTATE_RIGHT_PRESSED !PORTAbits.RA0
#define ROTATE_LEFT_ACTION IOCAFbits.IOCAF1
#define ROTATE_LEFT_PRESSED !PORTAbits.RA1
#define HARD_DROP_ACTION IOCAFbits.IOCAF4
#define HARD_DROP_PRESSED !PORTAbits.RA4
#define DEBOUNCER_ON T1CONbits.TMR1ON



#define PUYO_ONE 0x7F0E3F
#define PUYO_TWO 0x0082ff
#define PUYO_THREE 0xC21807

#endif	/* DEFINES_H */

